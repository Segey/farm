#!/usr/bin/python
# -*-coding: UTF-8 -*-

import os

os.system("wget \"https://nodejs.org/dist/v6.11.3/node-v6.11.3.tar.gz\" -O node.tar.gz")
if(os.path.exists("node")):
    os.system("sudo rm -R node")
os.system("mkdir node")
os.system("tar -xzf node.tar.gz -C node --strip-components=1")
os.system("cd node && ./configure")
os.system("cd node && sudo make install")

if(os.path.exists("node.tar.gz")):
    os.system("rm node.tar.gz")
if(os.path.exists("node")):
    os.system("sudo rm -R node")
print("--\nThe node.js has been installed!")
