# README #

### Mongodb structure ###
* The working branch is **master**

### Mongodb structure ###

* shops
* products

For more info see (src/server/models/Product.js and src/server/models/Shop.js) model data

### Versions ###

* node v6.11.3
* npm v5.4.2
* mongodb v3.2.17

### API ###
**GET /:shops**  
    Gets all shops

**GET /:shopName/:productId**  
    Get product by shopName and productId

**GET /admin/shop/:shopid**  
    Get shop by shopid

**POST /admin/shop/add**  
    Add new shop(send params[data: {}])
```
{
    "shopName" : "Cool",
    "url" : "https://niko-opt.com/price_two.csv",
    "parse_options" : {
        "newline" : "\r\n",
        "delimiter" : ";",
        "headers" : true,
        "ignoreEmpty" : true, 
        "encoding" : "cp1251"
    },
    "vendorcode" : [ 
        "Name"
    ]
}
/-----
{
    "shopName" : "Brave",
    "url" : "http://opt.10x10.com.ua/opt10x10.csv",
    "parse_options" : {
        "newline" : "\r\n",
        "delimiter" : ";",
        "headers" : true,
        "escape" : "\"",
        "quote" : "\"",
        "ignoreEmpty" : false,
        "encoding" : "cp1251"
    },
    "vendorcode" : [ 
        "name", 
        "sku"
    ]
}
```

**POST /:shopName**  
    Update price and add products to data
```
    localhost:8080/Cool/
    localhost:8080/Brave/
```
# Restrictions #
* Unique check for shops not supported

# How to run #

* install nodejs  (e.g. run python script install/./nodejs.py)
* install mongodb (e.g. run python script install/./mongodb.py)
* run mongodb (sudo systemctl start mongodb)
* cd into the project folder
* npm install
* fix etc/config.json if necessary(mongodb connect)
* npm run server (if an error occurs you need to install babel globally (e.g. npm install babel -g))
* npm run client 
* Open http://localhost:8090 in a browser
