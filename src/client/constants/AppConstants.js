import keyMirror from 'keymirror';

export default keyMirror({
      LOAD_SHOPS_REQUEST: null
    , LOAD_SHOPS_SUCCESS: null
    , LOAD_SHOPS_FAIL: null
    , LOAD_PRODUCTS_REQUEST: null
    , LOAD_PRODUCTS_SUCCESS: null
    , LOAD_PRODUCTS_FAIL: null
    , ADD_SHOP_SUCCESS: null
    , ADD_SHOP_FAIL: null
    , REFRESH_SHOP_SUCCESS: null
    , REFRESH_SHOP_FAIL: null
});
