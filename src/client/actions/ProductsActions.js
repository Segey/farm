import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../constants/AppConstants';

import api from '../api';

const ProductsActions = {
    findProduct(data) {
        AppDispatcher.dispatch({
            type: Constants.LOAD_PRODUCTS_REQUEST
        });

        api.findProduct(data).then(({ data }) =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_PRODUCTS_SUCCESS,
                product: data
            })
        )
        .catch(err =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_PRODUCTS_FAIL,
                error: err
            })
        );
    }
    , createShop(data) {
        AppDispatcher.dispatch({
            type: Constants.LOAD_PRODUCTS_REQUEST
        });

        api.createShop(data).then(({ data }) =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_PRODUCTS_SUCCESS,
                product: data
            })
        )
        .catch(err =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_PRODUCTS_FAIL,
                error: err
            })
        );
    }
};

export default ProductsActions;
