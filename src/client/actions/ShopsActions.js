import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../constants/AppConstants';

import api from '../api';

const ShopsActions = {
    loadShops() {
        AppDispatcher.dispatch({
            type: Constants.LOAD_SHOPS_REQUEST
        });

        api.listShops().then(({ data }) =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_SHOPS_SUCCESS,
                shops: data
            })
        )
        .catch(err =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_SHOPS_FAIL,
                error: err
            })
        );
    }
    , createShop(data) {
        AppDispatcher.dispatch({
            type: Constants.LOAD_PRODUCTS_REQUEST
        });

        api.createShop(data).then(({ data }) =>
            AppDispatcher.dispatch({
                type: Constants.ADD_SHOP_SUCCESS,
                shop: data 
            })
        )
        .catch(err =>
            AppDispatcher.dispatch({
                type: Constants.LOAD_SHOPS_FAIL,
                error: err
            })
        );
    }
    , parseShop(data) {
        AppDispatcher.dispatch({
            type: Constants.LOAD_PRODUCTS_REQUEST
        });

        api.parseShop(data).then(({ data }) =>
            AppDispatcher.dispatch({
                type: Constants.REFRESH_SHOP_SUCCESS,
                shop: data 
            })
        )
        .catch(err =>
            AppDispatcher.dispatch({
                type: Constants.REFRESH_SHOPS_FAIL,
                error: err
            })
        );
    }
};

export default ShopsActions;
