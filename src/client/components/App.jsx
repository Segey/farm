import React from 'react';
import AppStore from '../stores/AppStore';
import ProductsActions from '../actions/ProductsActions';
import ShopsActions from '../actions/ShopsActions';
import Masonry from 'react-masonry-component';

import './App.less';

function getStateFromFlux() {
    return {
          isLoading: AppStore.isLoading()
        , shops:     AppStore.getShops()
        , shop:      {shopName: ""}
        , product:   null
        , page:      1
        , productId: null 
        , error:     null
        , addedShop: AppStore.getAddedShop()
        , refreshFlag: false
        , refreshData: AppStore.getRefreshShop()
    };
}

const App = React.createClass({
    getInitialState() {
        return {
              isLoading:   false
            , shops:       []
            , product:      null
            , page:        1
        };
    }
    , componentWillMount() {
        ShopsActions.loadShops();
    }
    , componentDidMount() {
        AppStore.addChangeListener(this._onChange);
    }
    , componentWillUnmount() {
        AppStore.removeChangeListener(this._onChange);
    }
    , canShow(page) {
        return page === this.state.page ? "block" : "none";
    }
    , canShowProduct(val) {
        return (!!this.state.product && !!this.state.product[val]) ? "block" : "none";
    }
    , shopClick(shop) {
        this.setState({
               page : 2 
               , shop: shop
            });
    }
    , onBackClick() {
        this.setState(getStateFromFlux());
        ShopsActions.loadShops();
    }
    , onShowFullData() {
        const val = this.state.showFullData || false;
        this.setState({showFullData: !val});
    }
    , onPage3() {
        this.setState(getStateFromFlux());
        this.setState({ page : 3, addedShop: false, data: "" });
    }
    , onPage4() {
        this.setState(getStateFromFlux());
        this.setState({ page : 4, refresFlag: false, refreshData: "" });
    }
    , onSearch() {
        if(!!this.state.productId) {
            ProductsActions.findProduct( {
                "shopName": this.state.shop.shopName
                , "productId": this.state.productId
            });
        } else {
            this.setState({
                product: { statusCode: "The productId has not been passed." }
                , error: null
            });
        }
    }
    , onSearchFieldChange(event) {
        this.setState({productId: event.target.value});
    }
    , onAddShopChange(event) {
        this.setState({data: event.target.value, addedShop: false});
    }
    , onAddShopClick(event) {
        if(this.state.data) {
            ShopsActions.createShop({data: this.state.data});
            this.setState({ page : 3, addedShop: false, data: "" });
        }
    }
    , onRefreshChange(event) {
        this.setState({refreshData: event.target.value, refreshFlag: false});
    }
    , onRefreshClick(event) {
        if(this.state.refreshData) {
            ShopsActions.parseShop({shopName: this.state.refreshData});
            this.setState({ page : 4, refreshFlag: false, refreshData: "" });
        }
    }
    , render() {
        const masonryOptions = {
            itemSelector: '.shop',
            columnWidth: 250,
            gutter: 70,
            isFitWidth: true
        };
        const productOptions = {
            itemSelector: '.product',
            columnWidth: 600,
            gutter: 70,
            isFitWidth: true
        };
        const errorOptions = {
            itemSelector: '.error',
            columnWidth: 1200,
            gutter: 70,
            isFitWidth: true
        };

        return (
            <div className='app'>
                <div className="shops" style={{display: this.canShow(4) }}>
                    <button onClick={this.onBackClick}>back</button>
                    <h1 className="app-header">Refresh shop's products</h1>
                    <input onChange={this.onRefreshChange} value={this.state.refreshData} placeholder="a shop name"/>
                    <br />
                    <button onClick={this.onRefreshClick}>Refresh shop</button>
                    <div style={{display: this.state.refreshFlag ? "block" : "none"}}>
                        <hr />
                        <h4 className="app-header">The shop has been refreshed!</h4>
                    </div>
                </div>
                <div className="shops" style={{display: this.canShow(3) }}>
                    <button onClick={this.onBackClick}>back</button>
                    <h1 className="app-header">Add shop</h1>
                    <textarea onChange={this.onAddShopChange} value={this.state.data}/>
                    <br />
                    <button onClick={this.onAddShopClick}>Add shop</button>
                    <div style={{display: this.state.addedShop ? "block" : "none"}}>
                        <hr />
                        <h4 className="app-header">A new shop added</h4>
                    </div>
                </div>
                <div className="shops" style={{display: this.canShow(1) }}>
                    <button onClick={this.onPage3}>Add shop</button>
                    <button onClick={this.onPage4}>Refresh shop's products</button>
                    <h1 className='app-header'>Shops</h1>
                    <Masonry className='shops-grid' options={masonryOptions} >
                        {
                            this.state.shops.map(shop =>
                                <div className="shop" key={shop.id} onClick={() => this.shopClick(shop)}>
                                    {shop.shopName}
                                </div>
                            )
                        }
                    </Masonry>
                </div>
                <div className="products" style={{display: this.canShow(2) }}>
                    <button onClick={this.onBackClick}>back</button>
                    <h1 className='app-header'>Shop - {this.state.shop && this.state.shop.shopName}</h1>
                    <hr />
                    <h4 className='app-header'>Products</h4>
                    <div className="search">
                        <input type='search' placeholder="Enter id" onChange={this.onSearchFieldChange} value={this.state.productId}/>
                        <button onClick={this.onSearch}>Search</button>
                    </div>
                    <Masonry className='shops-grid' options={productOptions} style={{display: this.state.product ? "block" : "none"}}>
                        <div className="product">
                            <div style={{display: this.canShowProduct("statusCode") }}>{this.state.product && this.state.product.statusCode}</div>
                            <div style={{display: this.canShowProduct("_id") }}>Id: - {this.state.product && this.state.product._id}</div>
                            <div style={{display: this.canShowProduct("shopid") }}>ShopId: - {this.state.product && this.state.product.shopid}</div>
                            <div style={{display: this.canShowProduct("vendorCode") }} >VendorCode: - {this.state.product && this.state.product.vendorCode}</div>
                            <div style={{display: this.canShowProduct("Name") }} >Name: - {this.state.product && this.state.product.Name}</div>
                            <div style={{display: this.canShowProduct("createdAt") }} >CreatedAt: - {this.state.product && this.state.product.createdAt}</div>
                            <button className="plus" title="Show Full Data" onClick={this.onShowFullData}>+</button>

                            <div className="fulldata" style={{display: this.state.showFullData ? "block" : "none"}}>
                                <div>{this.state.showFullData && JSON.stringify(this.state.product, null, 2)}</div>
                            </div>
                        </div>
                    </Masonry>
                </div>
                    <Masonry className='shops-grid' options={productOptions} style={{display: this.state.error ? "block" : "none"}}>
                        <div className="error">
                            
                            <div className="error-title">
                                <h1>An error occurs</h1>
                                <h2>info</h2>
                            </div>
                            <div>{JSON.stringify(this.state.error, null, 2)}</div>
                        </div>
                    </Masonry>
            </div>
        );
    },

    _onChange() {
        this.setState({
            product :       AppStore.getProduct()
            , shops:        AppStore.getShops()
            , error:        AppStore.getLoadingError()
            , addedShop:    AppStore.getAddedShop()
            , refreshFlag:  AppStore.getRefreshShop()

        });
    }
});

export default App;
