import axios from 'axios';

import { apiPrefix } from '../../../etc/config.json';

export default {
    listShops() {
        return axios.get(`${apiPrefix}/shops`);
    }
    , findProduct(data) {
        return axios.get(`${apiPrefix}/${data.shopName}/${data.productId}/`);
    }
    , createShop(data) {
        return axios.post(`${apiPrefix}/admin/shop/add`, data);
    }
    , parseShop(data) {
        return axios.post(`${apiPrefix}/${data.shopName}`);
    }
}
