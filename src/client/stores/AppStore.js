import { EventEmitter } from 'events';

import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';

const CHANGE_EVENT = 'change';

let _shops = []
    , _loadingError = null
    , _isLoading = true
    , _addedShop = false
    , _product = null
    , _refreshShop = false;

function formatShop(shop) {
    return {
        id: shop._id
        , shopName: shop.shopName
        , createdAt: shop.createdAt
    };
}

const TasksStore = Object.assign({}, EventEmitter.prototype, {
    isLoading() {
        return _isLoading;
    }
    , getShops() {
        return _shops;
    }
    , getAddedShop() {
        return _addedShop;
    }
    , getRefreshShop() {
        return _refreshShop;
    }
    , getProduct() {
        return _product;
    }
    , getLoadingError() {
        return _loadingError;
    }
    , emitChange: function() {
        this.emit(CHANGE_EVENT);
    }
    , addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    }
    , removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function(action) {
    switch(action.type) {
        case AppConstants.LOAD_SHOPS_REQUEST: {
            _isLoading = true;

            TasksStore.emitChange();
            break;
        }
        case AppConstants.LOAD_SHOPS_SUCCESS: {
            _isLoading = false;
            _shops = action.shops.map( formatShop );
            _loadingError = null;

            TasksStore.emitChange();
            break;
        }
        case AppConstants.LOAD_SHOPS_FAIL: {
            _loadingError = action.error;

            TasksStore.emitChange();
            break;
        }

        case AppConstants.LOAD_PRODUCTS_REQUEST: {
            _isLoading = true;

            TasksStore.emitChange();
            break;
        }
        case AppConstants.LOAD_PRODUCTS_SUCCESS: {
            _isLoading = false;
            _product = action.product;
            _loadingError = null;

            TasksStore.emitChange();
            break;
        }
        case AppConstants.LOAD_PRODUCTS_FAIL: {
            _loadingError = action.error;
            _product = null;  

            TasksStore.emitChange();
            break;
        }
        case AppConstants.ADD_SHOP_SUCCESS: {
            _isLoading = false;
            _addedShop = true;
            _loadingError = null;

            TasksStore.emitChange();
            break;
        }
        case AppConstants.ADD_SHOP_FAIL: {
            _loadingError = action.error;
            _addedShop = false;  

            TasksStore.emitChange();
            break;
        }
        case AppConstants.REFRESH_SHOP_SUCCESS: {
            _isLoading = false;
            _refreshShop = true;
            _loadingError = null;

            TasksStore.emitChange();
            break;
        }
        case AppConstants.REFRESH_SHOP_FAIL: {
            _loadingError = action.error;
            _refreshShop = false;  

            TasksStore.emitChange();
            break;
        }

        default: {
            console.log('No such handler');
        }
    }
});

export default TasksStore;
