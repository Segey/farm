import mongoose from "mongoose";
import http     from 'http';
import https    from 'https';
import async    from 'async';
import URL      from 'url';
import csv      from "fast-csv";
import iconv    from "iconv";

import {ObjectID} from 'mongodb';
import config from '../../../etc/config.json';

import '../models/Product';
import '../models/Shop';

const Product = mongoose.model('Product');
const Shop = mongoose.model('Shop');

export function setUpConnection() {
    mongoose.Promise = require('bluebird');
    mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.name}`, { useMongoClient: true});
}

function valid(val) {
    return val !== null && val !== undefined;
}
function valid_array(val) {
    return valid(val) && val.length > 0;
}

export function listProducts(id) {
    return Product.find();
}
export function findProduct(name, id) {
    const details = { 'shopName': name };
    return Shop.findOne(details).then(data => {
        return Product.findById(id);
    });
}
export function listShops(id) {
    return Shop.find();
}
export function findShop(id) {
    return Shop.findById(id);
}
export function createShop(data) {
    const note = new Shop({
        url: data.url,
        shopName: data.shopName,
        parse_options: data.parse_options,
        vendorcode: data.vendorcode
    });
    return note.save();
}

var ParseShop = {
    shop: {}
    , toBool: function(val) {
        const falsy = /^(?:f(?:alse)?|no?|0+)$/i;
        return !falsy.test(val) && !!val;
    }
    , createOptions: function(options) {
        let opt = {};
        opt["delimiter"] = options["delimiter"];
        opt["headers"] = this.toBool(options["headers"]);
        opt["escape"] = options["escape"];
        opt["quote"] = options["quote"];
        opt["ignoreEmpty"] = this.toBool(options["ignoreEmpty"]);
        return opt;
    }
    , insertProducts: function(products, resolve, reject) {
        Product.collection.insert(products, (e, r) => {
            if(e) reject(e);
            else resolve();
        });
    }
    , removeOldProducts: function(products, resolve, reject) {
        const that = this
            , shopId = new ObjectID(that.shop._id)
            , data = {shopid: shopId};
        Product.collection.remove(data, (e, r) => {
            if(e) reject(e);
            else that.insertProducts(products, resolve, reject);
        });
    }
    , handleData: function(body, resolve, reject) {
        const that = this;
        var mdata = [];
        csv.fromString(body, that.createOptions(that.shop.parse_options))
            .transform(function(data){
                let code = "";
                try {
                    for(let i = 0; i != that.shop.vendorcode.length; ++i)
                        code += data[that.shop.vendorcode[i]];
                    data["vendorCode"] = code;
                    data["shopid"] = that.shop._id;
                    return data; 
                }
                catch(e) {
                    reject(e);
                }
             })
            .on("data", function(data){
                if(valid(data))
                    mdata.push(data);
             })
             .on("end", function(){
                 that.removeOldProducts(mdata, resolve, reject);
             });
    }
    , fetchData: function() {
        const that = this;
        return new Promise(function (resolve, reject) {
            const url = URL.parse(that.shop.url);
            if(!valid(url.host) || !valid(url.href))
                reject({ 'error': `Invalid path has been passed ${that.shop.url}`}); 

            const conv = new iconv.Iconv(that.shop.encoding || 'cp1251', 'utf8');
            (url.protocol === "http:" ? http : https).get(url, function(r) {
                let body = '';
                r.on('data', (chunk) => {
                    body += conv.convert(chunk).toString();
                });
                r.on('end', () => {
                    that.handleData(body, resolve, reject);
                });
            }).on('error', (e) => {
                reject({'error':`An Error occured while receive data from ${data.url}`}); 
            });
        });
    }
    , parse: function(name) {
        const details = { 'shopName': name }
            , that = this;

        return Shop.findOne(details).then(data => {
            that.shop = data;
            return that.fetchData();
        });
    }
}

export function parseShop(name) {
    return ParseShop.parse(name);
}
