import mongoose from "mongoose";

const Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId;

const ProductSchema = new Schema({
      vendorCode : { type: String, required: true }
    , shopId     : { type: ObjectId, required: true }
    , createdAt  : { type: Date, default: Date.now }
});

mongoose.model('Product', ProductSchema);
