import mongoose from "mongoose";

const Schema = mongoose.Schema;

const ShopSchema = new Schema({
      shopName  : { type: String, required: true }
    , url       : { type: String, required: true }
    , parse_options : {
          newline     : { type: String, required: true }
        , delimiter   : { type: String, required: true }
        , headers     : { type: Boolean, required: true }
        , escape      : { type: String}
        , quote       : { type: String}
        , encoding    : { type: String, default: "cp1251"}
        , ignoreEmpty : { type: Boolean, required: true }
    }
    , createdAt  : { type: Date, default: Date.now }
    , vendorcode : { type: Array, required: true }
});

mongoose.model('Shop', ShopSchema);
