import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { serverPort } from '../../etc/config.json';
import * as db from './utils/DataBaseUtils';

const app = express();
db.setUpConnection();
app.use( bodyParser.json() );
app.use(cors({ origin: '*' }));

// RESTful api handlers
app.get('/shops', (req, res) => {
    db.listShops()
        .then(data => res.send(data))
        .catch(err => res.status(400).send(err));
});
app.get('/admin/shop/:shopid', (req, res) => {
    db.findShop(req.params.shopid)
        .then(data => res.send(data))
        .catch(err => res.status(400).send(err));
});
app.post('/admin/shop/add', (req, res) => {
    var shop = undefined;
    try {
        shop = JSON.parse(req.body.data);
    }
    catch(e) {
        return res.status(400).send({ 'error': 'Invalid json' }); 
    }

    db.createShop(shop)
        .then(data => res.send(data))
        .catch(err => res.status(400).send(err));
});
app.get('/:shopName/:productId', (req, res) => {
    db.findProduct(req.params.shopName, req.params.productId)
        .then(data => {
            if(!!data)
                res.send(data);
            else res.send({
                statusCode: "the product has not been found"
                , shopName: req.params.shopName
                , productId: req.params.productId
            });
        })
        .catch(err => res.status(400).send(err));
});
app.post('/:shopName', (req, res) => {
    db.parseShop(req.params.shopName)
        .then(data => res.send({result: "done"}))
        .catch(err => res.status(400).send(err));
});

const server = app.listen(serverPort, () => {
    console.log(`Server is up and running on port ${serverPort}`);
});
